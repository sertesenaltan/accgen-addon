var actualCode = `
var gid = document.getElementById("captchagid").value;

function getvalue() {
    window.parent.postMessage(document.getElementsByClassName("g-recaptcha-response")[0].value + ";" + gid, "https://accgen.cathook.club"); //get recaptcha token and send to accgen
}

//element to render recaptcha in
document.getElementsByTagName("html")[0].innerHTML = '<div id="g-recaptcha" data-sitekey="6LerFqAUAAAAABMeByEoQX9u10KRObjwHf66-eya" data-callback="getvalue"></div>';
window.onload = function() {
    grecaptcha.ready(function() {
        grecaptcha.render("g-recaptcha", {
            sitekey: "6LerFqAUAAAAABMeByEoQX9u10KRObjwHf66-eya",
            callback: getvalue
        }); //render recaptcha with params
    })
}
`;    
var e = document.createElement("script");
e.appendChild(document.createTextNode(actualCode)), (document.head || document.documentElement).appendChild(e), e.parentNode.removeChild(e)
