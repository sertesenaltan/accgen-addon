function setHeader(e)
{
	console.log("ff h");
	for (var header of e.responseHeaders)
	{
		if (header.name.toLowerCase() === "x-frame-options")
		{
			header.value = "ALLOW";
		}
		else if (header.name.toLowerCase() === "content-security-policy")
		{
			header.value = header.value.replace(/frame-ancestors[^;]*;?/, "frame-ancestors http://* https://*;")
		}
	}
	var myHeader = {
		name: "x-frame-options",
		value: "ALLOW"
	};
	e.responseHeaders.push(myHeader);
	
	var myHeader2 = {
		name: "Access-Control-Allow-Origin",
		value: "https://accgen.cathook.club"
	};
	e.responseHeaders.push(myHeader2);
	
	return {
		responseHeaders: e.responseHeaders
	};
}


var HEADERS_TO_STRIP_LOWERCASE = [
	'content-security-policy',
	'x-frame-options',
];



try
{
	if (browser)
	{

		//firefox
		browser.webRequest.onBeforeSendHeaders.addListener(setHeader,
		{
			urls: ["https://accgen.cathook.club/*", "https://store.steampowered.com/*"]
		}, ["blocking", "requestHeaders"]);

		browser.webRequest.onHeadersReceived.addListener(setHeader,
		{
			urls: ["https://accgen.cathook.club/*", "https://store.steampowered.com/*"]
		}, ["blocking", "responseHeaders"]);



	}
}
catch (e)
{
	//chrome
	chrome.webRequest.onHeadersReceived.addListener(
		function (details)
		{
			for (var i = 0; i < details.responseHeaders.length; ++i) {
		if (details.responseHeaders[i].name.toLowerCase() === "Access-Control-Allow-Origin".toLowerCase()) {
			details.responseHeaders[i].value = "https://accgen.cathook.club";
			break;
		}
	}
	var myHeader2 = {
		name: "Access-Control-Allow-Origin",
		value: "https://accgen.cathook.club"
	};
	details.responseHeaders.push(myHeader2);
	
			return {
				responseHeaders: details.responseHeaders.filter(function (header)
				{
					return HEADERS_TO_STRIP_LOWERCASE.indexOf(header.name.toLowerCase()) < 0;
				})
			};
		},
		{
			urls: ["<all_urls>"]
		}, ["blocking", "responseHeaders","extraHeaders"]);

}